**Patterns in Keyword Ranks**

_Colab Link_:**_[RankApps](https://colab.research.google.com/drive/16CjWWTxkWXS80KVgYICiEL2vDRfilyEi?usp=sharing)_**

I have used here EDA and texthero for word pre-processing. Texthero is a good library and fast which helps us save time.
Then have used wordcloud and bar plot using the same which gave us overview of words which gave some insight.

Later, I have used Jaro Distance which helps us to measure the similarity between two strings.The higher the Jaro distance for two strings is, the more similar the strings are.

1. This data tells us multiple ranks are given to the same keywords or apps.
2. The similarity between apps and keywords are less than 50% ,more of that.
3. While, looking for answers, there are number of package which are ranked according to their keywords in package name.
4. This will basically boost their SEO as a whole, since few years people more and more losing the sight of having keyword ranking in title and     description as Google introduce an algorithm in 2015.
